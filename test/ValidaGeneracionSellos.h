#include <cxxtest/TestSuite.h>
#include <ClienteBD.h>
#include <string>
#include <stdlib.h>

class ValidaGeneracionSellos : public CxxTest::TestSuite {

    private:

        ClienteBD *bd = NULL;

        int ejecutarConsulta(string consulta) {
            if(!bd->estaConectado()) {
                return 0;
            }

            if(!bd->ejecutarProcedimientoAlmacenado(consulta)) {
                return 0;
            }

            return atoi(bd->resultado().c_str());
        }

    public:

        void setUp() {
            bd = new ClienteBD();
            bd->conectar();
        }

        void tearDown() {
            bd->desconectar();
            bd = NULL;
        }

        /**
         * Cuando el régimen enviado al procedimiento es nulo, retorna 0 (false)
         */
        void test_regimen_nulo(void) {
            string consulta("sp_valida_generacion_sellos(NULL, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento no existe, retorna 0 (false)
         */
        void test_regimen_no_existe(void) {
            string consulta("sp_valida_generacion_sellos(300032, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento no tiene asignado roles/obligaciones/actividades en la bd, 
         * en el ws igual, retorna 1 (true)
         */
        void test_regimen_no_tiene_roles_obligs_activs_en_bd(void) {
            string consulta("sp_valida_generacion_sellos(602, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento no tiene asignado roles/obligaciones/actividades en la bd,
         * pero en el ws sí, retorna 1 (true)
         */
        void test_regimen_no_tiene_roles_obligs_activs_en_bd_pero_si_en_ws(void) {
            string consulta("sp_valida_generacion_sellos(602, 'SET{10,20,30}', 'SET{100,200}', 'SET{1000}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles en la bd, y en el ws no, retorna 0 (false)
         */
        void test_regimen_tiene_roles_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_generacion_sellos(606, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles en la bd, y en el ws no coinciden,
         * retorna 0 (false)
         */
        void test_regimen_tiene_roles_en_bd_pero_en_ws_no_coinciden(void) {
            string consulta("sp_valida_generacion_sellos(606, 'SET{400050,400055}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles en la bd, y en el ws coincide por 
         * lo menos uno, retorna 1 (true)
         */
        void test_regimen_tiene_roles_en_bd_y_en_ws_coincide_minimo_uno(void) {
            string consulta("sp_valida_generacion_sellos(606, 'SET{300030,300031,300032,300033}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado obligaciones en la bd, y en el ws no, retorna 0 (false)
         */
        void test_regimen_tiene_obligs_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_generacion_sellos(624, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado obligaciones en la bd, y en el ws no coinciden,
         * retorna 0 (false)
         */
        void test_regimen_tiene_obligs_en_bd_pero_en_ws_no_coinciden(void) {
            string consulta("sp_valida_generacion_sellos(624, 'SET{}', 'SET{700,701,702,703,704,705}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado obligaciones en la bd, y en el ws coincide por 
         * lo menos uno, retorna 1 (true)
         */
        void test_regimen_tiene_obligs_en_bd_y_en_ws_coincide_minimo_uno(void) {
            string consulta("sp_valida_generacion_sellos(624, 'SET{}', 'SET{768,774}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles y obligaciones en la bd,
         * pero en el ws no, retorna 0 (false)
         */
        void test_regimen_tiene_roles_y_obligs_en_bd_pero_no_en_ws(void) {
            string consulta("sp_valida_generacion_sellos(622, 'SET{}', 'SET{}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles y obligaciones en la bd,
         * pero en el ws no coinciden ni los roles ni las oligaciones,
         * retorna 0 (false)
         */
        void test_regimen_tiene_roles_y_obligs_en_bd_pero_en_ws_no_coinciden_ambos(void) {
            string consulta("sp_valida_generacion_sellos(622, 'SET{1,2,3,4,5}', 'SET{700}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(0, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles y obligaciones en la bd, 
         * y en el ws coincide solo algún elemento en roles pero no en obligaciones, retorna 1 (true)
         */
        void test_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_minimo_un_rol(void) {
            string consulta("sp_valida_generacion_sellos(622, 'SET{300523,300524,300525}', 'SET{768,774}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles y obligaciones en la bd, 
         * y en el ws coincide solo algún elemento en obligaciones pero no en roles, retorna 1 (true)
         */
        void test_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_minimo_una_oblig(void) {
            string consulta("sp_valida_generacion_sellos(622, 'SET{300523}', 'SET{780,791}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }

        /**
         * Cuando el régimen enviado al procedimiento tiene asignado roles y obligaciones en la bd, 
         * y en el ws coincide algún elemento de roles y obligaciones, retorna 1 (true)
         */
        void test_regimen_tiene_roles_y_obligs_en_bd_y_en_ws_coincide_algun_elemento_de_roles_y_obligs(void) {
            string consulta("sp_valida_generacion_sellos(622, 'SET{30052}', 'SET{763}', 'SET{}')");

            int resultado = ejecutarConsulta(consulta);

            TS_ASSERT_EQUALS(1, resultado);
        }
};