#include <ClienteBD.h>

ClienteBD::ClienteBD() {
    db_info         = ITDBInfo();
    db_connection   = ITConnection();
    db_query        = NULL;
    db_row          = NULL;

    db_info.SetUser(ITString("informix"));
    db_info.SetDatabase(ITString("ar_sat"));
    db_info.SetSystem(ITString("pki_ar"));
    db_info.SetPassword(ITString("informix_123"));
}

ClienteBD::~ClienteBD() {
    liberar();
}

bool ClienteBD::conectar() {
    cout << "\nINFO: Conexión a la base de datos [dbservername:" << db_info.GetSystem().Data() << ",  dbname:" << db_info.GetDatabase().Data() <<"].\n";

    if(db_connection.IsOpen()) {
        return true;
    }
    else {
        if(db_connection.Open(db_info)) {
            return true;
        }
        else {
            cout << "ERROR: Conexión a la base de datos [" + string(db_connection.ErrorText().Data()) + "].\n";

            return false;
        }
    }
}

bool ClienteBD::desconectar() {
    cout << "INFO: Desconexión de la base de datos.\n";

    if(db_connection.Close()) {
        return true;
    }
    else {
        cout << "ERROR: Desconexión de la base de datos [" + string(db_connection.ErrorText().Data()) + "].\n";

        return false;
    }
}

bool ClienteBD::estaConectado() {
    return db_connection.IsOpen();
}

bool ClienteBD::ejecutarConsulta(string query) {
    cout << "INFO: Ejecución de consulta [" << query << "].\n";

    db_query = new ITQuery(db_connection);

    char buffer[512];

    if(db_query) {
        sprintf(buffer, "%s", query.c_str());

        db_row = db_query->ExecOneRow(ITString(buffer));

        if(!db_row->IsNull()) {
            return true;
        }
        else {
            cout << "ERROR: Ejecución de consulta [" + string(db_query->ErrorText().Data()) + "].\n";

            return false;
        }
    }
    else {
        cout << "ERROR: Ejecución de consulta [" + string(db_query->ErrorText().Data()) + "].\n";

        return false;
    }
}

bool ClienteBD::ejecutarProcedimientoAlmacenado(string stored_procedure) {
    string query = "EXECUTE PROCEDURE " + stored_procedure;

    if(ejecutarConsulta(query)) {
        return true;
    }
    else {
        return false;
    }
}

string ClienteBD::resultado() {
    ITValue *db_value = db_row->Column(0);

    if(!db_value->IsNull()) {
        cout << "INFO: Resultado de la consulta [" << db_value->Printable().Data() << "].\n";

        return string(db_value->Printable().Data());
    }
    else {
        cout << "INFO: Resultado de la consulta [0].\n";

        return string("0");
    }
}

void ClienteBD::liberar() {
    if(!db_row->IsNull()) {
        db_row->Release();
        db_row->SetNull();
    }

    if(db_query != NULL) {
        db_query->Finish();
        db_query = NULL;
    }
}