/**
 *	Valida si el régimen recibido permite generar sellos digitales
 *
 *	@param cve_regimen			La clave del régimen a validar
 *	@param cve_roles			Las claves de los roles asociados al régimen
 *	@param cve_obligaciones		Las claves de las obligaciones asociadas al régimen
 *	@param cve_actividades		Las claves de las actividades asociadas al régimen
 *	@return 					1 indica que la validación fue exitosa, 0 en caso contrario
 */
CREATE PROCEDURE "informix".sp_valida_generacion_sellos(cve_regimen INTEGER DEFAULT NULL, cve_roles SET(INTEGER NOT NULL) DEFAULT NULL, cve_obligaciones SET(INTEGER NOT NULL) DEFAULT NULL, cve_actividades SET(INTEGER NOT NULL) DEFAULT NULL)
	RETURNING INTEGER;

    -- Constantes booleanas
	DEFINE true INTEGER;	-- 1
	DEFINE false INTEGER;	-- 0

    -- Registros de la base de datos
    DEFINE bd_roles INTEGER;
    DEFINE bd_obligaciones INTEGER;
    DEFINE bd_actividades INTEGER;

    -- Variable de retorno
    DEFINE validacion INTEGER;

    -- 1 true, 0 false
    LET true  = 1;
    LET false = 0;

    -- El proceso de validación se establece a false
    LET validacion = false;

    -- Si la clave del régimen recibida es nula retorna false
    IF (cve_regimen IS NULL) THEN
        RETURN validacion;
    END IF;

    -- Si el régimen no existe en la base de datos retorna false
    IF ((SELECT COUNT(regimen_cve) FROM cat_regimen WHERE regimen_cve = cve_regimen) == 0) THEN
    	RETURN validacion;
    END IF;

    -- Consulta los roles, obligaciones y actividades asociadas al régimen en la base de datos
    SELECT COUNT(rol_cve) INTO bd_roles FROM cat_rol WHERE regimen_cve = cve_regimen;
    SELECT COUNT(oblig_cve) INTO bd_obligaciones FROM cat_obligacion WHERE regimen_cve = cve_regimen;
    SELECT COUNT(activ_cve) INTO bd_actividades FROM cat_actividad WHERE regimen_cve = cve_regimen;


	-- El régimen no tiene asociado roles/obligaciones/actividades
	IF (bd_roles == 0 AND bd_obligaciones == 0 AND bd_actividades == 0) THEN
		LET validacion = true;
	
	-- El régimen tiene asociado roles
	ELIF (bd_roles > 0 AND bd_obligaciones == 0 AND bd_actividades == 0) THEN
		IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

	-- El régimen tiene asociado obligaciones
	ELIF (bd_roles == 0 AND bd_obligaciones > 0 AND bd_actividades == 0) THEN
		IF ((SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

	-- El régimen tiene asociado actividades
	ELIF (bd_roles == 0 AND bd_obligaciones == 0 AND bd_actividades > 0) THEN
		IF ((SELECT COUNT(activ_cve) FROM cat_actividad WHERE regimen_cve = cve_regimen AND activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

    -- El régimen tiene asociado roles y obligaciones
    ELIF (bd_roles > 0 AND bd_obligaciones > 0 AND bd_actividades == 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

	-- El régimen tiene asociado roles y actividades
    ELIF (bd_roles > 0 AND bd_obligaciones == 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE regimen_cve = cve_regimen AND activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

	-- El régimen tiene asociado obligaciones y actividades
    ELIF (bd_roles == 0 AND bd_obligaciones > 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE regimen_cve = cve_regimen AND activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;

	-- El régimen tiene asociado roles, obligaciones y actividades
    ELIF (bd_roles == 0 AND bd_obligaciones > 0 AND bd_actividades > 0) THEN
        IF ((SELECT COUNT(rol_cve) FROM cat_rol WHERE regimen_cve = cve_regimen AND rol_cve IN (SELECT * FROM TABLE(cve_roles))) > 0 OR
        	(SELECT COUNT(oblig_cve) FROM cat_obligacion WHERE regimen_cve = cve_regimen AND oblig_cve IN (SELECT * FROM TABLE(cve_obligaciones))) > 0 OR
        	(SELECT COUNT(activ_cve) FROM cat_actividad WHERE regimen_cve = cve_regimen AND activ_cve IN (SELECT * FROM TABLE(cve_actividades))) > 0) THEN
			LET validacion = true;
		ELSE
			LET validacion = false;
		END IF;
	ELSE
		LET validacion = false;
	END IF;
    
	RETURN validacion;

END PROCEDURE;