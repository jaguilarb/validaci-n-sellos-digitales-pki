#include <ClienteBD.h>

int main(int argc, char *argv[]) {
    ClienteBD *bd = new ClienteBD();

    string consulta("sp_valida_generacion_sellos(606, 'SET{300040}', 'SET{}', 'SET{}')");

    if(bd->conectar()) {
        if(bd->ejecutarProcedimientoAlmacenado(consulta)) {
            cout << "INFO: El resultado de la ejecución del SP es " + bd->resultado() + ".\n";
        }

        bd->desconectar();
    }

    return 0;
}